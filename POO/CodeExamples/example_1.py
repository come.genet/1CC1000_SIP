import math

class Point:
  # Actually, x and y are not instance attributes, 
  # we'll see that later. But let's pretend they are.
  x = 0
  y = 0
  
  def radius(self):
    return math.sqrt(self.x ** 2 + self.y ** 2)

p = Point()
print(p.x)     # 0
p.x = 2
print(p.x)     # 2

p = Point()
p.x = 3
p.y = 4
print(p.radius())      # 5.0

