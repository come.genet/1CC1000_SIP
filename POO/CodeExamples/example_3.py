import math


class Point:
  def __init__(self, x, y):
    self.__x = x
    self.y = y

p = Point(3, 4)
#print(p.__x)          # Error
print(p._Point__x)    # 3
print(p.y)            # 4


p1 = Point(3, 4)
p2 = Point(3, 4)
print(id(p1))     # 4560754960 (for example)
print(id(p2))     # 4560754832 (for example)

print(p1 == p2)     # False

print(id(10))      # 4466282312 (for example)
print(type(10))    # <class 'int'>
print(type("10"))  # <class 'str'>


print(dir(10))       # ['__abs__', '__add__', ...
                     # ... 'real', 'to_bytes']
print(dir("10"))     # ['__add__', '__class__', ...
                     # ... 'upper', 'zfill']
print("ab".upper())  # AB
