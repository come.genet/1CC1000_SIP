from abc import ABC, abstractmethod

class ChemicalComponent(ABC):

    # Le constructeur. C'est la méthode qui est appelée lorsqu'on écrit
    # cc = ChemicalComponent("EssaiCC") (sauf que c'est interdit car la classe est abstraite)
    def __init__(self, name):
        self.__name = name

    # L'attribut d'instance __name est "privé" :
    # Comme son nom commence par "__" il ne peut pas être utilisé ailleurs 
    # que dans cette classe (à moins d'utiliser la notation "_ChemicalComponent__name").
    # Cela permet de décourager les clients d'y accéder directement et favorise donc
    # la bonne pratique de "l'encapsulation" facilitant les évolutions de code.

    # Les clients doivent néanmoins pouvoir consulter le nom
    # du composant chimique.
    # solution 1 : créer un "getter", une méthode publique (que les clients ont le droit d'appeler)
    # qui se contente de retourner la valeur de l'attribut privé :
    def get_name(self):
        return self.__name
    # solution 2, plus Pythonesque : utiliser le décorateur @property qui permet de définir des
    # méthodes getter pouvant être invoquées comme des attributs (sans "()") :
    @property
    def name(self):
        return self.__name
    # Un client disposant d'un composant chimique c pouvant utiliser pour afficher son nom:
    # print(p.get_name())
    # print(p.name)

    # Pour s'assurer que tous les composants chimiques auront une masse
    # on définit une méthode fournissant cette masse, même si à ce niveau
    # d'abstraction on n'est pas encore capable de calculer cette masse.
    # La méthode est donc abstraite, et pas conséquent la classe aussi.
    @abstractmethod
    def get_mass():
        pass

# Impossible de créer un composant chimique car la classe est abstraite :
##cc = ChemicalComponent("EssaiCC")
# TypeError: Can't instantiate abstract class ChemicalComponent with abstract method get_mass