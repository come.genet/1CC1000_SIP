from ChemicalElement import ChemicalElement

class Atom(ChemicalElement):

    def __init__(self, name, neutron_number):
        super().__init__(name)
        self.__neutron_number = neutron_number

    def get_neutron_number(self):
        return self.__neutron_number

    def get_mass(self):
        return self.get_atomic_number() + self.get_neutron_number()

    def __str__(self):
        return f"[{self.get_atomic_number()}/{self.get_mass()}]{self.get_symbol()}"

# La classe ne peut toujours pas être instanciée :
##deuterium = Atom("Deuterium", 1)
# TypeError: Can't instantiate abstract class Atom with abstract methods get_atomic_number, get_symbol

#print(deuterium.name) # accès au nom avec la propriété
#print(deuterium.get_name()) # accès au nom avec une méthode classique
#print(deuterium.get_neutron_number())
# On ne peut pas demander la masse de l'atome car son numéro atomique n'est toujours pas connu :
##print(deuterium.get_mass())
