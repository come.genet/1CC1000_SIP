import pandas as pd

import numpy as np
import matplotlib

#========#
# Series #
#========#
# one-dimensional array-like object containing data +
# index (customizable, not necessarily unique)

# Creation
#--------- 
# create Series from a list or a tuple
# default index starts at 0
data_list = ["Tom", "Bob", "Lea", "Ada", "Tom"]
data_tuple = ("Tom", "Bob", "Lea", "Ada", "Tom")
s = pd.Series(data_list)
s = pd.Series(data_tuple)
print(s)

# specify a custom index
data_index = ['a', 'b', 'c', 'd', 'e']
s = pd.Series(data_list, data_index)
print(s)

# index items don't have to be unique
data_index = ['a', 'b', 'a', 'b', 'e']
s = pd.Series(data_list, data_index)
print(s)

# create Series from a dictionary
data_dict = {"cook":"Tom", "driver":"Bob", "gardener":"Lea", "maid":"Ada"}
s = pd.Series(data_dict)
print(s)

# pandas uses np.nan to represent missing data
s = pd.Series(["Tom", "Bob", "Lea", np.nan, "Tom"])
print(s)

# select
#-------
# Index is used to select one or multiple values

data_list = ["Tom", "Bob", "Lea", "Ada", "Tom"]
data_index = ['a', 'b', 'a', 'b', 'e']
s = pd.Series(data_list, data_index)
print(s)
print(s['a'])
print(s[['a','e']])

s = pd.Series(range(10,15))
print(s)
print(s[2:4])

# Filtering
#----------
s = pd.Series(range(10,15))
print(s)
print(s[s > 12])

# Operations
#-----------
print(s + 5)

# Operations between Series
#--------------------------
# Operations between Series (+ - / *) align values based on their associated index values 
# –they need not be the same length. 
# The result index will be the sorted union of the two indexes.

s1 = pd.Series(range(1,4))
print(s1)
s2 = pd.Series([10, np.nan, 20, 30])
print(s2)
print(s2 + s1)

s1 = pd.Series(range(1,4), ["jan.", "fev.", "mar."])
print(s1)
s2 = pd.Series([10, np.nan, 20, 30])
print(s2)
print(s2 + s1)

#===========#
# DataFrame #
#===========#
# - Spreadsheet-like data structure
# - Row index and column index
# - !!! It’s not a good idea to have 2 columns with the same name
# - A column = a Series

# Creation
#---------
sales = pd.DataFrame(np.random.randint(0, 100, (4, 3)), index=["Q1","Q2","Q3","Q4"], columns=range(2017,2020))
print(sales)

grades = pd.DataFrame({'Maths': {"Bob":10, "Alice":13, "John": 11}, 'Sport': {"Bob":17, "Alice":15}, 'Hist.': {"Bob":7, "Alice":16, "John": 10}})
print(grades)

# Select
#-------
# Select by column name
print(grades['Maths'])

# Select by row name
print(grades.loc['Bob'])

# Select by row index
print(grades.iloc[0])

# Select multiple rows by index
print(grades.iloc[0:2])

# New column
#-----------
# direct assigment
grades["Litt."] = [14, 18, 16]
print(grades)

# computed values
grades["double sport"] = grades["Sport"]*2
print(grades)

# New row
#--------
ada = pd.DataFrame({"Maths": 12, "Sport": 13, "Hist.": 14, "Litt.": 9}, index=["Ada"])
print(pd.concat([grades, ada]))

# Operations
#-----------
print(grades.loc['Bob'].mean())

print(grades < 12)

grades[grades>16] = 20
print(grades)

# Plotting
#---------
import matplotlib.pyplot as plt
grades.plot(kind="bar", stacked=True)
plt.show()

